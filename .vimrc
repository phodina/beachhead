if &compatible
  set nocompatible               " disable compatibility to old-time vi
endif

call plug#begin('~/.vim/plugged')
"Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'junegunn/fzf.vim'
Plug 'terryma/vim-multiple-cursors'
Plug 'scrooloose/nerdtree'
Plug 'w0rp/ale'
Plug 'airblade/vim-gitgutter'
Plug 'junegunn/goyo.vim' 
call plug#end()


set showmatch               " show matching brackets.
set ignorecase              " case insensitive matching
set mouse=a                 " enable mouse
set hlsearch                " highlight search results
set tabstop=4               " number of columns occupied by a tab character
set softtabstop=4           " see multiple spaces as tabstops so <BS> does the right thing
set expandtab               " converts tabs to white space
set shiftwidth=4            " width for autoindents
set autoindent              " indent a new line the same amount as the line just typed
set number relativenumber   " add line numbers
set wildmode=longest,list   " get bash-like tab completions
"set cc=80                   " set an 80 column border for good coding style
filetype plugin indent on   " allows auto-indenting depending on file type
syntax on                   " syntax highlighting

let g:mapleader = ' '

" Plugin mapping
map ; :Files<CR> 	    " Search files using fzf 

map <C-o> :NERDTreeToggle<CR> " Open NerdTree window

" df: distraction free mode (using Goyo).
nnoremap <Leader>df :Goyo<CR>

" Plugin config
let g:deoplete#enable_at_startup = 1

let g:airline_theme='alduin' 
