![With guns blazing][beachhead]
# Beachhead
Plain management of the most important dot files across multiple machines. 

Previously I used [dotdrop] which is a Python script that tracks dot files in YAML config file and allows templating. However, that means I need Python and must have faith in the code as I don't have the time to review it. Also I keep forgetting the arguments to the commands. The key feature for me was templating but since I run only Linux machines that is no longer a requirement.


# Common configuration files

- [Vim][vim]
- [Zsh][zsh]
- [git][git]


[vim]:.vimrc
[zsh]:.zshrc
[git]:.gitconfig
[dotdrop]:https://github.com/deadc0de6/dotdrop
[beachhead]:beachhead.jpg
